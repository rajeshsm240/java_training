package com.student.service;

import com.student.model.College;
import com.student.model.Department;
import com.student.model.Student;

public interface CollegeService {
	public abstract Department searchDepartment(College college,int deptId);
	public abstract Student searchStudent(College college,int search);
}
