package com.student.service;

import com.student.model.College;
import com.student.model.Department;
import com.student.model.Student;
import com.student.service.CollegeService;

public class CollegeServiceImpl implements CollegeService {

	@Override
	public Department searchDepartment(College college, int deptId) {
		Department department = null;
		Department[] departmentArray=college.getDepartments();
		for (int i = 0; i < departmentArray.length; i++) {
			if(departmentArray[i].getDeptId()==deptId) {
				department=departmentArray[i];
			}
		}
		return department;
	}
	
	@Override
	public Student searchStudent(College college, int search) {
		Student student = null;
		Student[] studentArray=college.getStudents();
		for (int i = 0; i < studentArray.length; i++) {
			if(studentArray[i].getStudId()==search) {
				student=studentArray[i];
			}
		}
		return student;
	}

}
