package com.student.service;

import com.student.model.Department;
import com.student.model.Student;

public interface DepartmentService {
	public abstract Student searchStudent(Department department,int search);
}
