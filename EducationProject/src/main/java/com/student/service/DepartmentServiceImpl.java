package com.student.service;

import com.student.model.Department;
import com.student.model.Student;

public class DepartmentServiceImpl implements DepartmentService {

	@Override
	public Student searchStudent(Department department, int search) {
		Student student = null;
		Student[] studentArray=department.getStudents();
		for (int i = 0; i < studentArray.length; i++) {
			if(studentArray[i].getStudId()==search) {
				student=studentArray[i];
			}
		}
		return student;
	}

}
