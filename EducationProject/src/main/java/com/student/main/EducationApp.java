package com.student.main;

import com.student.model.College;
import com.student.model.Department;
import com.student.model.Student;
import com.student.service.CollegeService;
import com.student.service.CollegeServiceImpl;
import com.student.service.DepartmentService;
import com.student.service.DepartmentServiceImpl;

public class EducationApp {

	public static void main(String[] args) {
		Student student1 = new Student(1234,"Hello",22);
		Student student2 = new Student(4567,"World",22);
		Student student3 = new Student(7890,"Good",22);
		
		Department department = new Department();
		department.setDeptId(420);
		department.setDeptName("Computer science");
		
		Student[] students=new Student[3];
		students[0]=student1;
		students[1]=student2;
		students[2]=student3;
		department.setStudents(students);
		
		Department[] departments=new Department[1];
		
		College college =new College();
		college.setCollegeId(123);
		college.setCollegeName("BIET");
		college.setDepartments(departments);
		
		DepartmentService departmentService=new DepartmentServiceImpl();
		Student student=departmentService.searchStudent(department, 4567);
		if(student!=null) {
			System.out.println("Student Id : "+student.getStudId());
			System.out.println("Student Name : "+student.getStudName());
			System.out.println("Student Age : "+student.getAge());
			
		}
		
		CollegeService collegeService=new CollegeServiceImpl();
		Department department21=collegeService.searchDepartment(college, 420);
		if(department!=null) {
			System.out.println("Department Id : "+department21.getDeptId());
			System.out.println("Department Name : "+department21.getDeptName());	
		}
		
		Student student11=collegeService.searchStudent(college, 456);
		if(student11!=null) {
			System.out.println("Student Id : "+student11.getStudId());
			System.out.println("Student Name : "+student11.getStudName());
			System.out.println("Student Age : "+student11.getAge());
			
		}
		
		college=null;
		department=null;
		students=null;
		departmentService=null;
		collegeService=null;
	}

}
