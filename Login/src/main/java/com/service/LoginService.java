package com.service;

import com.model.Login;

public interface LoginService {
	public abstract Login[] createUser(Login[] users,Login user);
	public abstract Login searchUser(String userName,String password,Login[] users);
	public abstract Login[] updateUser(Login[] users, String oldUserName,
            String password, String newUserName) ;
	public abstract Login[] deleteUser(Login[] users,String userName,String password);
}
