package com.service;

import com.dao.LoginDao;
import com.dao.LoginDaoImpl;
import com.model.Login;
import com.service.LoginService;

public class LoginServiceImpl implements LoginService {

	
	public Login searchUser(String userName, String password,Login[] users) {
		Login login =null;
		if(userName.length()>5 && password.length()>5) {
			LoginDao user =new  LoginDaoImpl();
			login=user.loginUser(userName, password, users);
		}
		else
			System.out.println("The lenght of username and password "
					+ "should be more than 5");
		return login;
	}

	public Login[] createUser(Login[] users, Login user) {
		LoginDao newuser =new  LoginDaoImpl();
		Login[] newUser=newuser.createUser(users, user);
		System.out.println("New user created");
		return newUser;
	}

	public Login[] updateUser(Login[] users, String oldUserName,
			                   String password, String newUserName) {
		Login login =null;
		LoginDao user =new  LoginDaoImpl();
		if(oldUserName.length()>5 && password.length()>5) {
		}
		else
			System.out.println("The lenght of username and password "
					+ "should be more than 5");
		Login[] newList=user.updateUser(users,oldUserName,newUserName);
		System.out.println("Username updated");
		login=user.loginUser(newUserName, password, users);
		return newList;
	}

	public Login[] deleteUser(Login[] users, String userName, String password) {
		LoginDao user =new  LoginDaoImpl();
		Login[] newList = user.delete(users, userName);
		System.out.println("Size of new List : "+newList.length);
		return newList;
	}
		
}
