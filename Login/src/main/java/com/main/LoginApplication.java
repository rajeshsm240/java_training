package com.main;

import java.util.Scanner;
import com.model.Login;
import com.service.LoginService;
import com.service.LoginServiceImpl;

public class LoginApplication {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Select the operation to be performed");
		System.out.println("1 - create user");
		System.out.println("2 - read user");
		System.out.println("3 - update username");
		System.out.println("4 - delete user");
		int i=input.nextInt();
		
		Login user1 = new Login("Rajesh", "123456");
		Login user2 = new Login("Maanvi","567890");
		
		Login[] users=new Login[2];
		users[0]=user1;
		users[1]=user2;
		
		LoginService service= new LoginServiceImpl();
		
		switch(i) {
			case 1:
				users=service.createUser(users, new Login("hclemp","51966310"));  //create
				System.out.println(users.length);
				break;
			
			case 2:
				System.out.println("Enter Username");
				String userName=input.next();
				System.out.println("Enter password");
				String password=input.next();
				service.searchUser(userName, password,users);
				break;
				
			case 3:
				System.out.println("Enter Username");
				userName=input.next();
				System.out.println("Enter password");
				password=input.next();
			
				System.out.println("Enter new Username");
				String newUserName=input.next();
				users=service.updateUser(users, userName, password, newUserName);
				break;
				
			case 4:
				System.out.println("Enter Username");
				userName=input.next();
				System.out.println("Enter password");
				password=input.next();
				users=service.deleteUser(users, userName, password);
				break;
				
			default:
				System.out.println("Enter valid option");
		}
		users=null;
		service=null;
	}
}
