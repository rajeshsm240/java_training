package com.dao;

import com.dao.LoginDao;
import com.model.Login;

public class LoginDaoImpl implements LoginDao {

	public Login loginUser(String userName, String password, Login[] users) {
		int j=0;
		for (int i = 0; i < users.length; i++) {
			if(userName.equals(users[i].getUserName()) && 
				password.equals(users[i].getPassword())) {
				System.out.println("Welcome "+userName);
				j=1;
			}
		}
		if (j==0) {
			System.out.println("enter valid username and password");
		}
		return null;
	}

	public Login[] createUser(Login[] users, Login user) {
		Login[] newUser=new Login[users.length+1];
		int i;
		for (i = 0; i < newUser.length-1; i++) {
			newUser[i]=users[i];
		}
		newUser[i]=user;
		return newUser;
	}

	public Login[] updateUser(Login[] users, String oldUserName,String newUserName) {
		for(int i=0;i<users.length;i++) {
			if(users[i].getUserName().equals(oldUserName)) {
				users[i].setUserName(newUserName);
			}
		}
		return users;
	}

	public Login[] delete(Login[] users, String userName) {
		Login[] newList =new Login[users.length-1];
		int j=0;
		for (int i = 0; i < users.length; i++) {
			if(userName.equals(users[i].getUserName())) {
				for(j=0;j<i;j++) {
					newList[j]=users[j];
				}
				while(j<newList.length) {
					i++;
					newList[j++]=users[i];
				}
				break;
			}
		}
		return newList;
	}

}
