package com.dao;

import com.model.Login;

public interface LoginDao{
	public abstract Login[] createUser(Login[] users,Login user);
	public abstract Login loginUser(String userName,String password,Login[] users);
	public abstract Login[] updateUser(Login[] users,String oldUserName,String newUserName);
	public abstract Login[] delete(Login[] users, String userName);
}