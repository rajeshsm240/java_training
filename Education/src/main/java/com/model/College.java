package com.model;

import java.util.Set;

import com.model.Department;
import com.model.Student;

public class College {
	private int collegeId;
	private String collegeName;
	private Set<Department> departments;
	private Set<Student> students;
	
	public College() {
		super();
	}

	public College(int collegeId, String collegeName, Set<Department> departments, Set<Student> students) {
		super();
		this.collegeId = collegeId;
		this.collegeName = collegeName;
		this.departments = departments;
		this.students = students;
	}

	public int getCollegeId() {
		return collegeId;
	}

	public void setCollegeId(int collegeId) {
		this.collegeId = collegeId;
	}

	public String getCollegeName() {
		return collegeName;
	}

	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

	public Set<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(Set<Department> departments) {
		this.departments = departments;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}
}
