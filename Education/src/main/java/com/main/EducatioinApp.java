package com.main;

import java.util.HashSet;
import java.util.Set;

import com.model.College;
import com.model.Department;
import com.model.Student;
import com.service.CollegeService;
import com.service.CollegeServiceImpl;
import com.service.DepartmentService;
import com.service.DepartmentServiceImpl;

public class EducatioinApp {

	public static void main(String[] args) {
		public static void main(String[] args) {
			Student student1 = new Student(1234,"Hello",22);
			Student student2 = new Student(4567,"World",22);
			Student student3 = new Student(7890,"Good",22);
			
			Department department = new Department();
			department.setDeptId(420);
			department.setDeptName("Computer science");
			
			Set<Student> students = new HashSet();
			students.add(student1);
			students.add(student2);
			students.add(student3);
			department.setStudents(students);
			
			Set<Department> departments=new HashSet();
			((Set<Student>) department).add(department);
			
			College college =new College();
			college.setCollegeId(123);
			college.setCollegeName("BIET");
			college.setDepartments(departments);
			
			DepartmentService departmentService=new DepartmentServiceImpl();
			Student student=departmentService.searchStudent(department, 4567);
			if(student!=null) {
				System.out.println("Student Id : "+student.getStudId());
				System.out.println("Student Name : "+student.getStudName());
				System.out.println("Student Age : "+student.getAge());
				
			}
			
			CollegeService collegeService=new CollegeServiceImpl();
			Department department21=collegeService.searchDepartment(college, 420);
			if(department!=null) {
				System.out.println("Department Id : "+department21.getDeptId());
				System.out.println("Department Name : "+department21.getDeptName());	
			}
			
			Student student11=collegeService.searchStudent(college, 456);
			if(student11!=null) {
				System.out.println("Student Id : "+student11.getStudId());
				System.out.println("Student Name : "+student11.getStudName());
				System.out.println("Student Age : "+student11.getAge());
				
			}
			
			college=null;
			department=null;
			students=null;
			departmentService=null;
			collegeService=null;
		}
	}

}
