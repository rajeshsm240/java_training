package service;

import com.model.College;
import com.model.Department;
import com.model.Student;

public interface CollegeService {
	public abstract Department searchDepartment(College college,int deptId);
	public abstract Student searchStudent(College college,int search);
}
