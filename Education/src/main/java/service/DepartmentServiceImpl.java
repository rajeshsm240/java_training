package service;

import java.util.Iterator;
import java.util.Set;

import com.model.Department;
import com.model.Student;

public class DepartmentServiceImpl implements DepartmentService {
	
	@Override
	public Student searchStudent(Department department, int search) {
		Student student = null;
		Set<Student> studentColl=(Set<Student>) department.getStudents();

		for (Iterator iterator = studentColl.iterator(); iterator.hasNext();) {
			Student findStudent = (Student) iterator.next();
			if(findStudent.getStudId()==search) {
				student=findStudent;
			}
		}
		return student;
	}
	
}
