package service;

import java.util.Iterator;
import java.util.Set;

import com.model.College;
import com.model.Department;
import com.model.Student;

public class CollegeServiceImpl implements CollegeService {

	public Department searchDepartment(College college, int deptId) {
		Department department = null;
		Set<Department> departmentColl=college.getDepartments();
		for (Iterator iterator = departmentColl.iterator(); iterator.hasNext();) {
			Department department1 = (Department) iterator.next();
			if(department1.getDeptId()==deptId) {
				department=department1;
			}
		}
		return department;
	}

	public Student searchStudent(College college, int search) {
		Student student = null;
		Set<Student> studentColl=college.getStudents();
		for (Iterator iterator = studentColl.iterator(); iterator.hasNext();) {
			Student student1 = (Student) iterator.next();
			if(student1.getStudId()==search)
				student=student1;
		}
		return student;
	}

}
